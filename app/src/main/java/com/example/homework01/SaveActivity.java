package com.example.homework01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SaveActivity extends AppCompatActivity {


    TextView txtFullName, txtEmail, txtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save);


        txtFullName = findViewById(R.id.DetailName);
        txtEmail = findViewById(R.id.DetailEmail);
        txtPassword = findViewById(R.id.DetailPw);

        Intent intent = getIntent();
        String fullName = intent.getStringExtra("FullName");
        String email = intent.getStringExtra("Email");
        String password = intent.getStringExtra("Password");

        txtFullName.setText(fullName);
        txtEmail.setText(email);
        txtPassword.setText(password);

        Button passDataTargetReturnDataButton = findViewById(R.id.btnBack);
        passDataTargetReturnDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("message_return", "Enter new user information");
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}