package com.example.homework01;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button bCancel, bSave;

    EditText etFullName, etEmail, etPassword, etConfirmpassword;


    boolean isAllFieldsChecked = false;
    User user=new User();
    private final static int REQUEST_CODE_1 = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bSave = findViewById(R.id.proceedButton);
        bCancel = findViewById(R.id.cancelButton);


        etFullName = findViewById(R.id.fullName);
        etEmail = findViewById(R.id.email);
        etPassword = findViewById(R.id.password);
        etConfirmpassword = findViewById(R.id.confirmpassword);


        bSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isAllFieldsChecked = CheckAllFields();

                if (isAllFieldsChecked) {
                    user.setFullName(etFullName.getText().toString());
                    user.setEmail(etEmail.getText().toString());
                    user.setPassword(etPassword.getText().toString());

                    Intent intent = new Intent(MainActivity.this, SaveActivity.class);
                    intent.putExtra("FullName", user.fullName);
                    intent.putExtra("Email", user.email);
                    intent.putExtra("Password", user.password);
                    startActivityForResult(intent,REQUEST_CODE_1 );
                }
            }
        });

        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.this.finish();
                System.exit(0);
            }
        });
    }


   String Epattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    private boolean CheckAllFields() {
        if (etFullName.getText().toString().equals("")) {
            etFullName.setError("FullName is required!");
            return false;
        }

        if (etEmail.getText().toString().equals("")) {
            etEmail.setError("Email is required!");
            return false;
        }
        else if(!etEmail.getText().toString().matches((Epattern))){
            etEmail.setError("Invalid Email!");
            return false;
        }

        if (etPassword.getText().toString().equals("")) {
            etPassword.setError("Password is required!");
            return false;
        }

        if (etConfirmpassword.getText().toString().equals("")){
            etConfirmpassword.setError("Confirm password!");
            return false;
        }

        if (!etPassword.getText().toString().equals(etConfirmpassword.getText().toString()) ){
            etConfirmpassword.setError("Password do not match!");
            etPassword.setError("Password do not match!");
            return false;
        }

        return true;
    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case REQUEST_CODE_1:
                if(resultCode == RESULT_OK)
                {

                    String messageReturn = data.getStringExtra("message_return");
                    Context context = getApplicationContext();
                    CharSequence text = messageReturn;
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();

                    if (etFullName != null && etEmail != null && etPassword != null && etConfirmpassword != null) {
                        etFullName.setText("");
                        etEmail.setText("");
                        etPassword.setText("");
                        etConfirmpassword.setText("");
                    }
                }

        }
    }
}
